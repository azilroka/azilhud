﻿if not (IsAddOnLoaded('Tukui') or IsAddOnLoaded('AsphyxiaUI') or IsAddOnLoaded('DuffedUI')) then return end
local AH = unpack(select(2,...))
local oUF = AH.oUF

HUDNames = {}
OtherUnitFrames = {
	'TukuiPlayer',
	'TukuiTarget',
	'TukuiTargetTarget',
	'TukuiFocus',
	'TukuiFocusTarget',
	'TukuiPet',
	'DuffedUIPlayer',
	'DuffedUITarget',
	'DuffedUITargetTarget',
	'DuffedUIFocus',
	'DuffedUIFocusTarget',
	'DuffedUIPet',
	'oUF_Player',
	'oUF_Target',
	'oUF_TargetTarget',
	'oUF_Focus',
	'oUF_FocusTarget',
	'oUF_Pet',
	'oUF_TukuiPlayer',
	'oUF_TukuiTarget',
	'oUF_TukuiTargetTarget',
	'oUF_TukuiFocus',
	'oUF_TukuiFocusTarget',
	'oUF_TukuiPet',
}

local function BuildHUD(self, unit)
	self:EnableMouse(true)
	self:SetFrameStrata('BACKGROUND')
	self:RegisterForClicks('AnyUp')
	self:RegisterEvent('PLAYER_ENTERING_WORLD', AH.UpdateAllElements)
	self:SetScript('OnEnter', UnitFrame_OnEnter)
	self:SetScript('OnLeave', UnitFrame_OnLeave)
	tinsert(HUDNames, self:GetName())

	--Everything has Health/Power/Name
	AH:CreateHealthBar(self)
	AH:CreatePowerBar(self)

	self.Health.Value = AH:CreateTagLayout(self)
	self:Tag(self.Health.Value, '[HealthPercent]')

	self.Power.Value = AH:CreateTagLayout(self)
	self:Tag(self.Power.Value, '[PowerPercent]')

	if HUDOptions.ShowValues then
		self.Health.Value:Show()
		self.Power.Value:Show()
	else
		self.Health.Value:Hide()
		self.Power.Value:Hide()
	end

	self.Name = AH:CreateTagLayout(self)
	self.Name:Point('TOP', self.Health, 'BOTTOM', 0, -8)
	self:Tag(self.Name, '[NameColor][name]\n[DifficultyColor][smartlevel]\n[PlayerStatus]')

	self.MinMaxHP = AH:CreateTagLayout(self, true)
	self.MinMaxHP:Point('TOP', self.Name, 'BOTTOM', 0, 0)
	self:Tag(self.MinMaxHP, '[curhp] / [maxhp]')

	-- self.FadeCasting = true
	-- self.FadeCombat = true
	-- self.FadeTarget = false
	-- self.FadeHealth = true
	-- self.FadePower = true
	-- self.FadeHover = true
	-- self.FadeSmooth = 0.5
	-- self.FadeMinAlpha = 0.3
	-- self.FadeMaxAlpha = 1

	if unit == 'player' then
		self.Health:Point('LEFT', self, 'LEFT')
		self.Health.Value:Point('TOPRIGHT', self.Health, 'TOPLEFT', -20, -15)

		self.Power.FadeBar = CreateFrame('StatusBar', nil, self.Power)
		self.Power.FadeBar:SetOrientation('VERTICAL')
		self.Power.FadeBar:SetFrameLevel(self.Power:GetFrameLevel())
		self.Power.FadeBar:SetAllPoints()
		self.Power.FadeBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		self.Power:Point('LEFT', self.Health, 'RIGHT', 7, 0)
		self.Power.Value:Point('TOPLEFT', self.Power, 'TOPRIGHT', 20, -15)

		AH:CreateCombatFeedback(self)
		self.CombatFeedbackText:SetPoint('LEFT', self.Power, 'RIGHT', 20, 0)

		AH:CreateIndicators(self)
		self.Combat:Point('CENTER', self.Health, 'BOTTOMRIGHT', 2, 40)
		self.PvP:Point('CENTER', self.Health, 'CENTER', 0, 0)
		self.Leader:Point('CENTER', self.Health, 'TOPLEFT', 0, 0)
		self.MasterLooter:Point('CENTER', self.Health, 'TOPRIGHT', 0, 0)
		self.Resting:Point('BOTTOM', self.Health, 'TOP', 0, 0)

		AH:CreateCastBar(self)
		self.Castbar:Point('LEFT', self.Power, 'RIGHT', 7, 0)
		self.Castbar.Button:Point('BOTTOMLEFT', self.Castbar.Backdrop, 'BOTTOMRIGHT', 2, 0)
		self.Castbar.Text:Point('LEFT', self.Castbar.Button, 'RIGHT', 2, 0)
		self.Castbar.Time:Point('BOTTOM', self.Castbar, 'TOP', 0, 4)

		AH:CreateExperienceBar(self)
		AH:CreateReputationBar(self)

		self.Experience:Point('LEFT', self.Health, 'RIGHT', 7, 0)
		self.Reputation:Point('LEFT', self.Health, 'RIGHT', 7, 0)
		
		self:HookScript('OnEnter', function()
			if UnitInVehicle('player') then return end
			self.Power:Hide()
			if UnitLevel('player') ~= MAX_PLAYER_LEVEL or not IsXPUserDisabled() then
				self.Experience:Show()
			else
				self.Reputation:Show()
			end
		end)
		self:HookScript('OnLeave', function()
			if UnitInVehicle('player') then return end
			self.Power:Show()
			self.Experience:Hide()
			self.Reputation:Hide()
		end)

		local Tank, Heal, DPS = UnitGetAvailableRoles(unit)
		if Tank then
			AH:CreateVengeanceBar(self)
		end
		local StatueBarPoint
		if AH.MyClass == 'DEATHKNIGHT' then
			AH:CreateRuneBar(self)
			self.Runes:Point('RIGHT', self.Health, 'LEFT', -7, 0)
			StatueBar, StatueBarPoint = true, self.Runes
		elseif AH.MyClass == 'DRUID' then
			AH:CreateEclipseBar(self)
			self.EclipseBar:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			AH:CreateDruidManaBar(self)
			self.DruidMana:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			AH:CreateWildMushroomBar(self)
		elseif AH.MyClass == 'MAGE' then
			AH:CreateRunePowerBar(self)
			self.RunePower:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			AH:CreateArcaneChargeBar(self)
			self.ArcaneChargeBar:SetScript('OnShow', function(frame)
				frame:ClearAllPoints()
				if self.RuneOfPower:IsShown() then
					frame:Point('RIGHT', self.RuneOfPower, 'LEFT', -7, 0)
				else
					frame:Point('RIGHT', self.Health, 'LEFT', -7, 0)
				end
			end)
		elseif AH.MyClass == 'MONK' then
			AH:CreateHarmonyBar(self)
			self.HarmonyBar:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			StatueBar, StatueBarPoint = true, self.HarmonyBar
		elseif AH.MyClass == 'PALADIN' then
			AH:CreateHolyPowerBar(self)
			self.HolyPower:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		elseif AH.MyClass == 'PRIEST' then
			AH:CreateShadowOrbsBar(self)
			self.ShadowOrbsBar:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			AH:CreateWeakendSoulBar(self)

			StatueBar, StatueBarPoint = true, self.ShadowOrb
		--[[elseif AH.MyClass == 'ROGUE' then
			AH:CreateAnticipationBar(self)
			self.AnticipationBar:Point('RIGHT', self.Health, 'LEFT', -7, 0)

			AH:CreateBanditsGuileBar(self)
			self.BanditsGuileBar]]
		elseif AH.MyClass == 'SHAMAN' then
			AH:CreateTotemBar(self)
			self.TotemBar:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		elseif AH.MyClass == 'WARLOCK' then
			AH:CreateWarlockSpecBars(self)
			self.WarlockSpecBars:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		elseif AH.MyClass == 'WARRIOR' then
			StatueBar, StatueBarPoint = true, self.Health
		end
		if StatueBar then
			AH:CreateStatueBar(self)
			self.Statue:Point('RIGHT', StatueBarPoint, 'LEFT', -7, 0)
		end
	elseif unit == 'target' then
		self.Health:Point('RIGHT', self, 'RIGHT')
		self.Health.Value:Point('BOTTOMLEFT', self.Health, 'BOTTOMRIGHT', 20, 0)

		self.Power:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		self.Power.Value:Point('BOTTOMRIGHT', self.Power, 'BOTTOMLEFT', -20, 0)

		AH:CreateCombatFeedback(self)
		self.CombatFeedbackText:SetPoint('RIGHT', self.Power, 'LEFT', -20, 0)

		if AH.MyClass == 'PRIEST' then
			AH:CreateWeakendSoulBar(self)
		end

		AH:CreateIndicators(self)
		self.PvP:Point('CENTER', self.Health, 'CENTER', 0, 0)
		self.RaidIcon:Point('CENTER', self.Health, 'TOP', 0, 0)
		self.ClassIcon:SetPoint('RIGHT', self.Name, 'LEFT', -5, 0)

		AH:CreateCastBar(self)
		self.Castbar:Point('RIGHT', self.Power, 'LEFT', -7, 0)
		self.Castbar.Text:Point('TOP', self.Castbar, 'BOTTOM', 0, -35)
		self.Castbar.Time:Point('BOTTOM', self.Castbar, 'TOP', 0, 4)

		AH:CreateComboBar(self)
		self.ComboPointsBar:Point('LEFT', self.Health, 'RIGHT', 7, 0)
	elseif unit == 'targettarget' then
		self.Health:Point('RIGHT', self, 'RIGHT')
		self.Health.Value:Point('LEFT', self.Health, 'RIGHT', 6, 0)

		self.Power:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		self.Power.Value:Point('RIGHT', self.Power, 'LEFT', -4, 0)
	elseif unit == 'focus' then
		self.Health:Height(self.Health:GetHeight() * .75)
		self.Health:Point('LEFT', self, 'LEFT')
		self.Health.Value:Point('RIGHT', self.Health, 'LEFT', -4, 0)

		self.Power:Height(self.Power:GetHeight() * .75)
		self.Power:Point('LEFT', self.Health, 'RIGHT', 7, 0)
		self.Power.Value:Point('LEFT', self.Power, 'RIGHT', 4, 0)
	elseif unit == 'focustarget' then
		self.Health:Height(self.Health:GetHeight() * .75)
		self.Health:Point('RIGHT', self, 'RIGHT')
		self.Health.Value:Point('LEFT', self.Health, 'RIGHT', 6, 0)

		self.Power:Height(self.Power:GetHeight() * .75)
		self.Power:Point('RIGHT', self.Health, 'LEFT', -7, 0)
		self.Power.Value:Point('RIGHT', self.Power, 'LEFT', -4, 0)
	else
		self.Health:Height(self.Health:GetHeight() * .75)
		self.Health:Point('LEFT', self, 'LEFT')
		self.Health.Value:Point('RIGHT', self.Health, 'LEFT', -4, 0)

		self.Power:Height(self.Power:GetHeight() * .75)
		self.Power:Point('LEFT', self.Health, 'RIGHT', 7, 0)
		self.Power.Value:Point('LEFT', self.Power, 'RIGHT', 4, 0)
	end
	if unit == 'pet' then
		self:RegisterEvent('UNIT_PET', AH.UpdateAllElements)
	end
end

local HUDLoader = CreateFrame('Frame')
HUDLoader:RegisterEvent('ADDON_LOADED')
HUDLoader:SetScript('OnEvent', function(self, event, name)
	if name ~= 'AzilHUD' then return end
	if HUDOptions == nil then
		HUDOptions = {
			ShowValues = true,
			HideUF = true,
			HealthHeight = 150,
			HealthWidth = 20,
			CastbarHeight = 150,
			CastbarWidth = 10,
			ClassBarHeight = 150,
			ClassBarWidth = 6,
			PowerHeight = 150,
			PowerWidth = 6,
			Font = AH.UFFont,
			FontSize = 14,
			FontFlag = 'NONE',
			FeedbackSize = 20,
			Texture = 'Asphyxia',
		}
	end

	oUF:RegisterStyle('AzilHUD', BuildHUD)
	oUF:SetActiveStyle('AzilHUD')

	local Player = oUF:Spawn('player', 'AzilHUD_Player')
	Player:Point('RIGHT', UIParent, 'CENTER', -150, 0)
	Player:Size(HUDOptions.HealthWidth + HUDOptions.ClassBarWidth + HUDOptions.PowerWidth, HUDOptions.HealthHeight)

	local Target = oUF:Spawn('target', 'AzilHUD_Target')
	Target:Point('LEFT', UIParent, 'CENTER', 150, 0)
	Target:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight)

	local TargetTarget = oUF:Spawn('targettarget', 'AzilHUD_TargetTarget')
	TargetTarget:Point('LEFT', Target, 'RIGHT', 80, 0)
	TargetTarget:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight*.75)

	local Pet = oUF:Spawn('pet', 'AzilHUD_Pet')
	Pet:Point('RIGHT', Player, 'LEFT', -120, 0)
	Pet:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight*.75)

	local PetTarget = oUF:Spawn('pettarget', 'AzilHUD_PetTarget')
	PetTarget:Point('LEFT', Pet, 'RIGHT', 40, 0)
	PetTarget:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight*.75)

	local Focus = oUF:Spawn('focus', 'AzilHUD_Focus')
	Focus:Point('RIGHT', Player, 'LEFT', -320, 0)
	Focus:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight*.75)

	local FocusTarget = oUF:Spawn('focustarget', 'AzilHUD_FocusTarget')
	FocusTarget:Point('LEFT', Focus, 'RIGHT', 40, 0)
	FocusTarget:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight*.75)
	
	for _, object in pairs(OtherUnitFrames) do
		if HUDOptions['HideUF'] then
			if _G[object] then _G[object]:Disable() end
		end
	end
end)

SLASH_AZILHUD1 = '/hud'
SlashCmdList['AZILHUD'] = function(arg)
	if arg == 'enable' then
		for _, object in pairs(HUDNames) do
			_G[object]:Enable()
		end
		print(AH.Title, 'is enabled.')
	elseif arg == 'disable' then
		for _, object in pairs(HUDNames) do
			_G[object]:Disable()
		end
		print(AH.Title, 'is disabled.')
	elseif arg == 'options' then
		if HUDOptionsFrame:IsShown() then
			HUDOptionsFrame:Hide()
		else
			HUDOptionsFrame:Show()
		end
	else
		print(AH.Title, 'Options')
		print('/hud options - Options GUI')
		print('/hud enable 	- Enables the HUD')
		print('/hud disable - Temporarily Disables the HUD')
	end
end