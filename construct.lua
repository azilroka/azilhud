﻿if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local AH = unpack(select(2,...))
local oUF = AH.oUF
local MyClass = AH.MyClass

ClassBar = {}
ClassBarElements = {}

function AH:CreateHealthBar(Frame)
	local Health = CreateFrame('StatusBar', Frame:GetName()..'_Health', Frame)
	Health:Size(HUDOptions.HealthWidth, HUDOptions.HealthHeight)
	Health:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Health:SetOrientation('VERTICAL')
	Health:CreateBackdrop('Transparent')
	Health.Backdrop:CreateShadow()

	Health:SetStatusBarColor(.3, .3, .3)
	Health.frequentUpdates = true
	Health.Smooth = true

	local MyBar = CreateFrame('StatusBar', nil, Health)
	MyBar:SetOrientation('VERTICAL')
	MyBar:SetPoint('LEFT')
	MyBar:SetPoint('RIGHT')
	MyBar:SetPoint('BOTTOM', Health:GetStatusBarTexture(), 'TOP', 0, 0)
	MyBar:SetHeight(HUDOptions.HealthHeight)
	MyBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	MyBar:SetStatusBarColor(0, 0.3, 0.15, 1)
	MyBar:SetMinMaxValues(0,1)

	local OtherBar = CreateFrame('StatusBar', nil, Health)
	OtherBar:SetOrientation('VERTICAL')
	OtherBar:SetPoint('LEFT')
	OtherBar:SetPoint('RIGHT')
	OtherBar:SetPoint('BOTTOM', Health:GetStatusBarTexture(), 'TOP', 0, 0)
	OtherBar:SetHeight(HUDOptions.HealthHeight)
	OtherBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	OtherBar:SetStatusBarColor(0, 0.3, 0, 1)

	local AbsorbBar = CreateFrame('StatusBar', nil, Health)
	AbsorbBar:SetOrientation('VERTICAL')
	AbsorbBar:SetPoint('LEFT')
	AbsorbBar:SetPoint('RIGHT')
	AbsorbBar:SetPoint('BOTTOM', Health:GetStatusBarTexture(), 'TOP', 0, 0)
	AbsorbBar:SetHeight(HUDOptions.HealthHeight)
	AbsorbBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	AbsorbBar:SetStatusBarColor(0.3, 0.3, 0, 1)

	OtherBar:SetFrameLevel(AbsorbBar:GetFrameLevel() + 1)
	MyBar:SetFrameLevel(AbsorbBar:GetFrameLevel() + 2)

	Frame.HealPrediction = {
		myBar = MyBar,
		otherBar = OtherBar,
		absorbBar = AbsorbBar,
		maxOverflow = 1,
	}

	Health.PostUpdate = AH.PostUpdateHealth
	Frame.Health = Health
end

function AH:CreatePowerBar(Frame)
	local Power = CreateFrame('StatusBar', Frame:GetName()..'_Power', Frame)
	Power:Size(HUDOptions.PowerWidth, HUDOptions.PowerHeight)
	Power:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Power:SetOrientation('VERTICAL')
	Power:CreateBackdrop('Transparent')
	Power.Backdrop:CreateShadow()

	Power.colorClass = true
	Power.colorReaction = true
	Power.frequentUpdates = true
	Power.Smooth = true

	Power.PreUpdate = AH.PreUpdatePower
	Power.PostUpdate = AH.PostUpdatePower

	Frame.Power = Power
end

function AH:CreateVengeanceBar(Frame)
	local Vengeance = CreateFrame('StatusBar', Frame:GetName()..'_Vengeance', Frame)
	Vengeance:Size(HUDOptions.PowerWidth, HUDOptions.PowerHeight)
	Vengeance:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Vengeance:CreateBackdrop('Transparent')
	Vengeance.Backdrop:CreateShadow()

	Vengeance:FontString('Value', HUDOptions.Font, HUDOptions.FontSize, HUDOptions.FontFlag)
	Vengeance.Value:SetPoint('CENTER', Vengeance, 'CENTER', 0, 0)
	Vengeance.Value:SetTextColor(1, 1, 1, 1)

	Vengeance.Orientation = 'VERTICAL'
	Vengeance.Usetext = 'perc'

	Frame.Vengeance = Vengeance
end

function AH:CreateCastBar(Frame)
	local Castbar = CreateFrame('StatusBar', Frame:GetName()..'_Castbar', Frame)
	Castbar:Size(HUDOptions.CastbarWidth, HUDOptions.CastbarHeight)
	Castbar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Castbar:SetOrientation('VERTICAL')
	Castbar:CreateBackdrop('Transparent')
	Castbar.Backdrop:CreateShadow()

	Castbar:FontString('Time', HUDOptions.Font, HUDOptions.FontSize, HUDOptions.FontFlag)
	Castbar.Time:SetJustifyH('RIGHT')

	Castbar:FontString('Text', HUDOptions.Font, HUDOptions.FontSize, HUDOptions.FontFlag)
	Castbar.Text:SetJustifyH('RIGHT')

	Castbar.Button = CreateFrame('Frame', nil, Castbar)
    Castbar.Button:CreateShadow()
	Castbar.Button:SetTemplate()
	Castbar.Button:Size(24)

	Castbar.Icon = Castbar.Button:CreateTexture(nil, 'ARTWORK')
	Castbar.Icon:SetInside()
	Castbar.Icon:SetTexCoord(0.08, 0.92, 0.08, .92)

	Castbar.SafeZone = Castbar:CreateTexture(nil, 'BORDER')
	Castbar.SafeZone:SetTexture(AH.BarPath..HUDOptions.Texture)
	Castbar.SafeZone:SetVertexColor(0.8, 0.2, 0.2, 0.75)

	Castbar.PostCastStart = AH.CheckCast
	Castbar.PostChannelStart = AH.CheckChannel

	Frame.Castbar = Castbar
end

function AH:CreateIndicators(Frame)
	local Assistant = Frame.Health:CreateTexture(nil, 'OVERLAY')
	Assistant:Size(12)

	local ClassIcon = Frame.Health:CreateTexture(nil, 'OVERLAY')
	ClassIcon:Size(28)

	local Combat = Frame.Health:CreateTexture(nil, 'OVERLAY')
	Combat:Size(20)
	Combat:SetTexture(AH.IconPath..'CombatSwords')

	local Leader = Frame.Health:CreateTexture(nil, 'OVERLAY')
	Leader:Size(12)

	local LFDRole = Frame.Health:CreateTexture(nil, 'OVERLAY')
	LFDRole:Size(28)
	LFDRole.PostUpdate = AH.PostUpdateLFDRole

	local MasterLooter = Frame.Health:CreateTexture(nil, 'OVERLAY')
	MasterLooter:Size(12)
	MasterLooter:SetTexture(AH.IconPath..'MasterLooter')

	local PvP = Frame.Health:CreateTexture(nil, 'OVERLAY')
	PvP:Size(28)
	PvP.PostUpdate = AH.PostUpdatePvP

	local RaidIcon = Frame.Health:CreateTexture(nil, 'OVERLAY')
	RaidIcon:SetTexture(AH.IconPath..'RaidIcons')
	RaidIcon:Size(28)

	local ReadyCheck = Frame.Health:CreateTexture(nil, 'OVERLAY')
	ReadyCheck:Size(32)

	local Resting = Frame.Health:CreateTexture(nil, 'OVERLAY')
	Resting:Size(32)
	Resting:SetTexture(AH.IconPath..'Resting')

	local ResurrectIcon = Frame.Health:CreateTexture(nil, 'OVERLAY')
	ResurrectIcon:Point('CENTER', Frame.Health, 'CENTER')
	ResurrectIcon:Size(30, 25)
	ResurrectIcon:SetDrawLayer('OVERLAY', 7)

	local Threat = Frame.Health:CreateTexture(nil, 'OVERLAY')
	Threat:Size(32)

	Frame.Assistant = Assistant
	Frame.ClassIcon = ClassIcon
	Frame.Combat = Combat
	Frame.Leader = Leader
	Frame.LFDRole = LFDRole
	Frame.MasterLooter = MasterLooter
	Frame.PvP = PvP
	Frame.RaidIcon = RaidIcon
	Frame.ReadyCheck = ReadyCheck
	Frame.Resting = Resting
	Frame.ResurrectIcon = ResurrectIcon
	Frame.Threat = Threat
end

function AH:CreateExperienceBar(Frame)
	local Experience = CreateFrame('StatusBar', Frame:GetName()..'_Experience', Frame)
	Experience:EnableMouse(true)
	Experience:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Experience:SetOrientation('VERTICAL')
	Experience:SetStatusBarColor(0, 0.4, 1, 0.8)
	Experience:Size(HUDOptions.PowerWidth, HUDOptions.PowerHeight)
	Experience:CreateBackdrop('Transparent')
	Experience.Backdrop:CreateShadow()
	Experience:Hide()

	Experience.Rested = CreateFrame('StatusBar', Frame:GetName()..'_Experience_Rested', Experience)
	Experience.Rested:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Experience.Rested:SetOrientation('VERTICAL')
	Experience.Rested:SetAllPoints(Experience)
	Experience.Rested:SetStatusBarColor(1, 0, 1, 0.2)
	Experience:SetScript('OnEnter', function(self)
		if UnitInVehicle('player') then return end
		Frame.Power:Hide()
		Experience:Show()
		local min, max, rested = UnitXP('player'), UnitXPMax('player'), GetXPExhaustion()
		GameTooltip:SetOwner(self, 'ANCHOR_BOTTOM', 0, -5)
		GameTooltip:AddLine(format(XP..': %d / %d (%d%% - %d/%d)', min, max, min/max * 100, 20 - (20 * (max - min) / max), 20))
		GameTooltip:AddLine(format(LEVEL_ABBR..': %d (%d%% - %d/%d)', max - min, (max - min) / max * 100, 20 * (max - min) / max, 20))
		if rested ~= nil then
			GameTooltip:AddLine(format('|cff0090ff'..TUTORIAL_TITLE26..': +%d (%d%%)', rested, rested / max * 100))
		end
		GameTooltip:Show()
	end)
	Experience:SetScript('OnLeave', function()
		if UnitInVehicle('player') then return end
		Frame.Power:Show()
		Experience:Hide()
		GameTooltip:Hide()
	end)

	Experience.PostUpdate = Experience.Hide
	Frame.Experience = Experience
end

function AH:CreateReputationBar(Frame)
	local Reputation = CreateFrame('StatusBar', Frame:GetName()..'_Reputation', Frame)
	Reputation:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	Reputation:SetOrientation('VERTICAL')
	Reputation:Size(HUDOptions.PowerWidth, HUDOptions.PowerHeight)
	Reputation:CreateBackdrop('Transparent')
	Reputation.Backdrop:CreateShadow()
	Reputation:Hide()

	Reputation:SetScript('OnEnter', function()
		if UnitInVehicle('player') then return end
		Frame.Power:Hide()
		Reputation:Show()
	end)
	Reputation:SetScript('OnLeave', function()
		if UnitInVehicle('player') then return end
		Frame.Power:Show()
		Reputation:Hide()
	end)

	Reputation.colorStanding = true
	Reputation.PostUpdate = Reputation.Hide
	Frame.Reputation = Reputation
end

function AH:CreateCombatFeedback(Frame)
	local CombatFeedbackText = Frame:FontString(nil, HUDOptions.Font, HUDOptions.FeedbackSize, HUDOptions.FontFlag)
	CombatFeedbackText.colors = {
		DAMAGE = { 0.69, 0.31, 0.31 },
		CRUSHING = { 0.69, 0.31, 0.31 },
		CRITICAL = { 0.69, 0.31, 0.31 },
		GLANCING = { 0.69, 0.31, 0.31 },
		STANDARD = { 0.84, 0.75, 0.65 },
		IMMUNE = { 0.84, 0.75, 0.65 },
		ABSORB = { 0.84, 0.75, 0.65 },
		BLOCK = { 0.84, 0.75, 0.65 },
		RESIST = { 0.84, 0.75, 0.65 },
		MISS = { 0.84, 0.75, 0.65 },
		HEAL = { 0.33, 0.59, 0.33 },
		CRITHEAL = { 0.33, 0.59, 0.33 },
		ENERGIZE = { 0.31, 0.45, 0.63 },
		CRITENERGIZE = { 0.31, 0.45, 0.63 },
	}

	Frame.CombatFeedbackText = CombatFeedbackText
end

function AH:CreateTagLayout(Frame, Highlight)
	local Tag = Frame:FontString(nil, HUDOptions.Font, HUDOptions.FontSize, HUDOptions.FontFlag)
	Tag:SetDrawLayer(Highlight and 'HIGHLIGHT' or 'OVERLAY')
	Tag:SetJustifyV('MIDDLE')
	Tag:SetJustifyH('CENTER')

	return Tag
end

function AH:CreateComboBar(Frame)
	local ComboPointsBar = CreateFrame('Frame', Frame:GetName()..'_ComboPointsBar', Frame)
	ComboPointsBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	ComboPointsBar:CreateBackdrop('Transparent')
	ComboPointsBar.Backdrop:CreateShadow()
	tinsert(ClassBar, ComboPointsBar:GetName())

	for i = 1, 5 do					
		ComboPointsBar[i] = CreateFrame('StatusBar', Frame:GetName()..'_ComboPointsBar_'..i, ComboPointsBar)
		ComboPointsBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		ComboPointsBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, ComboPointsBar[i]:GetName())

		if i == 1 then
			ComboPointsBar[i]:Point('BOTTOM', ComboPointsBar)
			ComboPointsBar[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 5)	
		else
			ComboPointsBar[i]:Point('BOTTOM', ComboPointsBar[i-1], 'TOP', 0, 1)
			ComboPointsBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 5) - 1)
		end
	end

	Frame.ComboPointsBar = ComboPointsBar
end

function AH:CreateStatueBar(Frame)
	local StatueBar = CreateFrame('StatusBar', Frame:GetName()..'_StatueBar', Frame)
	StatueBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	StatueBar:SetOrientation('VERTICAL')
	StatueBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	StatueBar:CreateBackdrop('Transparent')
	StatueBar.Backdrop:CreateShadow()
	tinsert(ClassBar, StatueBar:GetName())

	Frame.Statue = StatueBar
end

function AH:CreateRuneBar(Frame)
	local Runes = CreateFrame('Frame', Frame:GetName()..'_RuneBar', Frame)
	Runes:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	Runes:CreateBackdrop('Transparent')
	Runes.Backdrop:CreateShadow()
	tinsert(ClassBar, Runes:GetName())

	for i = 1, 6 do
		Runes[i] = CreateFrame('StatusBar', Frame:GetName()..'_RuneBar_Rune'..i, Runes)
		Runes[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		Runes[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, Runes[i]:GetName())

		if i == 1 then
			Runes[i]:Point('BOTTOM', Runes)
			Runes[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 6)
		else
			Runes[i]:Point('BOTTOM', Runes[i-1], 'TOP', 0, 1)
			Runes[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 6) - 1)
		end
	end
	
	Frame.Runes = Runes
end

function AH:CreateEclipseBar(Frame)
	local EclipseBar = CreateFrame('Frame', Frame:GetName()..'_EclipseBar', Frame)
	EclipseBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	EclipseBar:CreateBackdrop('Transparent')
	EclipseBar.Backdrop:CreateShadow()
	tinsert(ClassBar, EclipseBar:GetName())

	local LunarBar = CreateFrame('StatusBar', Frame:GetName()..'_LunarBar', EclipseBar)
	LunarBar:Point('BOTTOM', EclipseBar, 'BOTTOM', 0, 0)
	LunarBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	LunarBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	LunarBar:SetStatusBarColor(.30, .52, .90)
	LunarBar:SetOrientation('VERTICAL')
	tinsert(ClassBar, LunarBar:GetName())

	local SolarBar = CreateFrame('StatusBar', Frame:GetName()..'_SolarBar', EclipseBar)
	SolarBar:Point('BOTTOM', LunarBar:GetStatusBarTexture(), 'TOP', 0, 0)
	SolarBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	SolarBar:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	SolarBar:SetStatusBarColor(.80, .82,  .60)
	SolarBar:SetOrientation('VERTICAL')
	tinsert(ClassBar, SolarBar:GetName())

	EclipseBar.LunarBar = LunarBar
	EclipseBar.SolarBar = SolarBar
	Frame.EclipseBar = EclipseBar
end

function AH:CreateDruidManaBar(Frame)
	local DruidMana = CreateFrame('StatusBar', Frame:GetName()..'_DruidMana', Frame)
	DruidMana:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	DruidMana:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	DruidMana:SetOrientation('VERTICAL')
	DruidMana:CreateBackdrop('Transparent')
	DruidMana.Backdrop:CreateShadow()
	DruidMana:SetStatusBarColor(0.30, 0.52, 0.90)
	tinsert(ClassBar, DruidMana:GetName())

	Frame.DruidMana = DruidMana
end

function AH:CreateWildMushroomBar(Frame)
	local WildMushroom = CreateFrame('Frame', Frame:GetName()..'_WildMushroomBar', Frame)
	WildMushroom:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	WildMushroom:CreateBackdrop('Transparent')
	WildMushroom.Backdrop:CreateShadow()
	tinsert(ClassBar, WildMushroom:GetName())

	for i = 1, 3 do
		WildMushroom[i] = CreateFrame('StatusBar', Frame:GetName()..'_WildMushroomBar_Mushroom'..i, WildMushroom)
		WildMushroom[i]:SetOrientation('VERTICAL')
		WildMushroom[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		WildMushroom[i]:SetStatusBarColor(212/255, 212/255, 212/255)
		tinsert(ClassBarElements, WildMushroom[i]:GetName())

		if i == 1 then
			WildMushroom[i]:Point('BOTTOM', WildMushroom)
			WildMushroom[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 3)
		else
			WildMushroom[i]:Point('BOTTOM', WildMushroom[i - 1], 'TOP', 0, 1)
			WildMushroom[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 3) - 1)
		end
	end

	WildMushroom.PostUpdate = AH.PostUpdateWildMushroom
	Frame.WildMushroom = WildMushroom
end

function AH:CreateArcaneChargeBar(Frame)
	local ArcaneChargeBar = CreateFrame('Frame', Frame:GetName()..'_ArcaneChargeBar', Frame)
	ArcaneChargeBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	ArcaneChargeBar:CreateBackdrop('Transparent')
	ArcaneChargeBar.Backdrop:CreateShadow()
	tinsert(ClassBar, ArcaneChargeBar:GetName())

	for i = 1, 6 do
		ArcaneChargeBar[i] = CreateFrame('StatusBar', Frame:GetName()..'_ArcaneChargeBar_Charge'..i, ArcaneChargeBar)
		ArcaneChargeBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		ArcaneChargeBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, ArcaneChargeBar[i]:GetName())

		if i == 1 then
			ArcaneChargeBar[i]:Point('BOTTOM', ArcaneChargeBar)
			ArcaneChargeBar[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 6)
		else
			ArcaneChargeBar[i]:Point('BOTTOM', ArcaneChargeBar[i - 1], 'TOP', 0, 1)
			ArcaneChargeBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 6) - 1)
		end
	end

	Frame.ArcaneChargeBar = ArcaneChargeBar
end

function AH:CreateRunePowerBar(Frame)
	local RunePower = CreateFrame('Frame', Frame:GetName()..'_RunePower', Frame)
	RunePower:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	RunePower:CreateBackdrop('Transparent')
	RunePower.Backdrop:CreateShadow()
	tinsert(ClassBar, RunePower:GetName())

	for i = 1, 2 do
		RunePower[i] = CreateFrame('StatusBar', Frame:GetName()..'_RunePowerBar_Rune'..i, RunePower)
		RunePower[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		RunePower[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, RunePower[i]:GetName())

		if i == 1 then
			RunePower[i]:Point('BOTTOM', RunePower)
			RunePower[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 2)
		else
			RunePower[i]:Point('BOTTOM', RunePower[i - 1], 'TOP', 0, 1)
			RunePower[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 2) - 1)
		end
	end

	Frame.RunePower = RunePower
end

function AH:CreateHarmonyBar(Frame)
	local HarmonyBar = CreateFrame('Frame', Frame:GetName()..'_HarmonyBar', Frame)
	HarmonyBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	HarmonyBar:CreateBackdrop('Transparent')
	HarmonyBar.Backdrop:CreateShadow()
	tinsert(ClassBar, HarmonyBar:GetName())

	for i = 1, 5 do
		HarmonyBar[i] = CreateFrame('StatusBar', Frame:GetName()..'_HarmonyBar_Harmony'..i, HarmonyBar)
		HarmonyBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		HarmonyBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, HarmonyBar[i])

		if i == 1 then
			HarmonyBar[i]:Point('BOTTOM', HarmonyBar)
		else
			HarmonyBar[i]:Point('BOTTOM', HarmonyBar[i-1], 'TOP', 0, 1)
		end
	end
	HarmonyBar.PostUpdate = AH.PostUpdateHarmony

	Frame.HarmonyBar = HarmonyBar
end

function AH:CreateHolyPowerBar(Frame)
	local HolyPower = CreateFrame("Frame", Frame:GetName().."_HolyPowerBar", Frame)
	HolyPower:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	HolyPower:CreateBackdrop("Transparent")
	HolyPower.Backdrop:CreateShadow()
	tinsert(ClassBar, HolyPower:GetName())

	for i = 1, 5 do
		HolyPower[i] = CreateFrame('StatusBar', Frame:GetName()..'_HolyPowerBar_HolyPower'..i, HolyPower)
		HolyPower[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		HolyPower[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, HolyPower[i]:GetName())

		if i == 1 then
			HolyPower[i]:Point('BOTTOM', HolyPower)
		else
			HolyPower[i]:Point('BOTTOM', HolyPower[i-1], 'TOP', 0, 1)
		end
	end

	HolyPower.PostUpdate = AH.PostUpdateHolyPower
	Frame.HolyPower = HolyPower
end

function AH:CreateWeakendSoulBar(Frame)
	local WeakenedSoul = CreateFrame('StatusBar', Frame:GetName()..'_WeakenedSoul', Frame)
	WeakenedSoul:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
	WeakenedSoul:SetOrientation('VERTICAL')
	WeakenedSoul:SetAllPoints(Frame.Power)
	WeakenedSoul:SetStatusBarColor(191/255, 10/255, 10/255)

	Frame.WeakenedSoul = WeakenedSoul
end

function AH:CreateShadowOrbsBar(Frame)
	local ShadowOrbsBar = CreateFrame('Frame', Frame:GetName()..'_ShadowOrbsBar', Frame)
	ShadowOrbsBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	ShadowOrbsBar:CreateBackdrop('Transparent')
	ShadowOrbsBar.Backdrop:CreateShadow()
	tinsert(ClassBar, ShadowOrbsBar:GetName())

	for i = 1, 3 do					
		ShadowOrbsBar[i] = CreateFrame('StatusBar', Frame:GetName()..'_ShadowOrbsBar_ShadowOrb'..i, ShadowOrbsBar)
		ShadowOrbsBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		ShadowOrbsBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, ShadowOrbsBar[i]:GetName())

		if i == 1 then
			ShadowOrbsBar[i]:Point('BOTTOM', ShadowOrbsBar)
			ShadowOrbsBar[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 3)
		else
			ShadowOrbsBar[i]:Point('BOTTOM', ShadowOrbsBar[i-1], 'TOP', 0, 1)
			ShadowOrbsBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 3) - 1)
		end
	end

	Frame.ShadowOrbsBar = ShadowOrbsBar
end

function AH:CreateAnticipationBar(Frame)
	local AnticipationBar = CreateFrame("Frame", Frame:GetName().."_AnticipationBar", Frame)
	AnticipationBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	AnticipationBar:CreateBackdrop("Transparent")
	AnticipationBar.Backdrop:CreateShadow()
	tinsert(ClassBar, AnticipationBar:GetName())

	for i = 1, 5 do
		AnticipationBar[i] = CreateFrame("StatusBar", Frame:GetName().."_AnticipationBar_"..i, AnticipationBar)
		AnticipationBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		AnticipationBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, AnticipationBar[i]:GetName())

		if i == 1 then
			AnticipationBar[i]:Point("BOTTOM", AnticipationBar)
			AnticipationBar[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 5)
		else
			AnticipationBar[i]:Point("BOTTOM", AnticipationBar[i-1], "TOP", 0, 1)
			AnticipationBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 5) - 1)
		end
	end

	Frame.AnticipationBar = AnticipationBar
end

function AH:CreateBanditsGuileBar(Frame)
	local BanditsGuileBar = CreateFrame("Frame", Frame:GetName().."_BanditsGuile", Frame)
	BanditsGuileBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	BanditsGuileBar:CreateBackdrop("Transparent")
	BanditsGuileBar.Backdrop:CreateShadow()
	tinsert(ClassBar, BanditsGuileBar:GetName())
	
	for i = 1, 3 do					
		BanditsGuileBar[i] = CreateFrame("StatusBar", Frame:GetName().."_BanditsGuile_"..i, BanditsGuileBar)
		BanditsGuileBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		BanditsGuileBar[i]:SetOrientation('VERTICAL')
		tinsert(ClassBarElements, BanditsGuileBar[i]:GetName())

		if i == 1 then
			BanditsGuileBar[i]:Point("BOTTOM", BanditsGuileBar)
			BanditsGuileBar[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / 3)
		else
			BanditsGuileBar[i]:Point("BOTTOM", BanditsGuileBar[i-1], "TOP", 0, 1)
			BanditsGuileBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 3) - 1)
		end
	end

	Frame.BanditsGuileBar = BanditsGuileBar
end

function AH:CreateTotemBar(Frame)
	local TotemBar = CreateFrame("Frame", Frame:GetName().."_TotemBar", Frame)
	TotemBar:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	TotemBar:CreateBackdrop()
	TotemBar.Backdrop:CreateShadow()
	tinsert(ClassBar, TotemBar:GetName())

	for i = 1, 4 do
		TotemBar[i] = CreateFrame("StatusBar", Frame:GetName().."_TotemBar"..i, TotemBar)
		TotemBar[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		TotemBar[i]:SetOrientation('VERTICAL')
		TotemBar[i]:SetMinMaxValues(0, 1)
		tinsert(ClassBarElements, TotemBar[i]:GetName())

		if i == 1 then
			TotemBar[i]:Point("BOTTOM", TotemBar)
			TotemBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 4) - 2)
		else
			TotemBar[i]:Point("BOTTOM", TotemBar[i-1], "TOP", 0, 1)
			TotemBar[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 4) - 1)
		end
		TotemBar[i].bg = TotemBar[i]:CreateTexture(nil, "BORDER")
		TotemBar[i].bg:SetAllPoints(TotemBar[i])
		TotemBar[i].bg:SetTexture(AH.BarPath..HUDOptions.Texture)
		TotemBar[i].bg.multiplier = 0.3
	end

	TotemBar.Destroy = true
	Frame.TotemBar = TotemBar
end

function AH:CreateWarlockSpecBars(Frame)
	local WarlockSpecBars = CreateFrame("Frame", Frame:GetName().."_WarlockSpecBar", Frame)
	WarlockSpecBars:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	WarlockSpecBars:CreateBackdrop("Transparent")
	WarlockSpecBars.Backdrop:CreateShadow()
	tinsert(ClassBar, WarlockSpecBars:GetName())

	for i = 1, 4 do
		WarlockSpecBars[i] = CreateFrame("StatusBar", Frame:GetName().."_WarlockSpecBar"..i, WarlockSpecBars)
		WarlockSpecBars[i]:SetStatusBarTexture(AH.BarPath..HUDOptions.Texture)
		WarlockSpecBars[i]:SetOrientation('VERTICAL')

		if i == 1 then
			WarlockSpecBars[i]:Point("BOTTOM", WarlockSpecBars)
		else
			WarlockSpecBars[i]:Point("BOTTOM", WarlockSpecBars[i-1], "TOP", 0, 1)
		end
		tinsert(ClassBarElements, WarlockSpecBars[i]:GetName())
	end

	WarlockSpecBars.PostUpdate = AH.PostUpdateWarlockBars
	Frame.WarlockSpecBars = WarlockSpecBars
end