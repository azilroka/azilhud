﻿if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local AH = unpack(select(2,...))
local oUF = AH.oUF

local UnitPowerType, UnitCanAttack, UnitIsPlayer = UnitPowerType, UnitCanAttack, UnitIsPlayer
local ipairs, unpack = ipairs, unpack

function AH:UpdateAllElements()
	for _, element in ipairs(self.__elements) do
		element(self, "UpdateElement", self.unit)
	end
end

function AH:PostUpdateHealth(unit, min, max)
	local r, g, b = oUF.ColorGradient(min, max, 1, 0.1, 0.1, 0.6, 0.3, 0.3, 0.3, 0.3, 0.3)
	self:SetStatusBarColor(r, g, b)
end

function AH:PreUpdatePower(unit)
	local pType, pToken = UnitPowerType(unit)
	local color = oUF.colors.power[pToken]

	self:SetStatusBarColor(unpack(color))
end

function AH:PostUpdatePower(unit, min, max)
	local pType, pToken = UnitPowerType(unit)
	local color = oUF.colors.power[pToken]

	if max == 0 and not UnitIsPlayer(unit) then
		self:Hide()
		self.Value:Hide()
	else
		if not unit == 'player' then
			self:Show()
			self.Value:Show()
		end
	end
end

function AH:PostUpdatePvP(PVPStatus)
	if PVPStatus == 'ffa' then
		self:SetTexture(AH.IconPath..'FFA')
	elseif PVPStatus ~= nil then
		self:SetTexture(AH.IconPath..PVPStatus)
	end
end

function AH:PostUpdateLFDRole(Role)
	self:SetTexCoord(0, 1, 0, 1)
	if Role == 'TANK' then
		self:SetTexture(AH.IconPath..'Tank')
	elseif Role == 'HEALER' then
		self:SetTexture(AH.IconPath..'Healer')
	elseif Role == 'DAMAGER' then
		self:SetTexture(AH.IconPath..'DPS')
	end
end

function AH:CheckInterrupt(self, unit)
	if (unit == "vehicle") then
		unit = "player"
	end

	if (self.interrupt and UnitCanAttack("player", unit)) then
		self:SetStatusBarColor(1, 0, 0, 0.5)
	else
		self:SetStatusBarColor(.3, .3, .3)
	end
end

function AH:UpdateSafeZone(self, channel)
	local SafeZone = self.SafeZone
	local Height = self:GetHeight()
	local _, _, _, MS = GetNetStats()
	SafeZone:ClearAllPoints()
	if channel then SafeZone:SetPoint('BOTTOM') else SafeZone:SetPoint('TOP') end
	SafeZone:SetPoint('LEFT')
	SafeZone:SetPoint('RIGHT')

	if(MS ~= 0) then
		local Percent = (Height / self.max) * (MS / 1e5)
		if(Percent > 1) then Percent = 1 end
		SafeZone:SetHeight(Height * Percent)
		SafeZone:Show()
	else
		SafeZone:Hide()
	end
end

function AH:CheckCast(unit, name, castid)
	AH:CheckInterrupt(self, unit)
	AH:UpdateSafeZone(self)
end

function AH:CheckChannel(unit, name)
	AH:CheckInterrupt(self, unit)
	AH:UpdateSafeZone(self, true)
end

function AH:PostUpdateWildMushroom()
	local Frame = self:GetParent()
	local EclipseBar = Frame.EclipseBar
	local DruidMana = Frame.DruidMana
	local Anchor = Frame.Health

	if DruidMana:IsShown() then
		Anchor = DruidMana
	end

	if EclipseBar:IsShown() then
		Anchor = EclipseBar
	end

	self:ClearAllPoints()
	self:Point('RIGHT', Anchor, 'LEFT', -7, 0)
end

function AH:PostUpdateHarmony()
	local max = UnitPowerMax("player", SPELL_POWER_CHI)
	for i = 1, 5 do
		if i == 1 then
			self[i]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight / max)
		else
			self[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / max) - 1)
		end
	end
end

function AH:PostUpdateHolyPower()
	for i = 1, 5 do
		self[i]:Width(HUDOptions.ClassBarWidth)
	end
end

function AH:PostUpdateWarlockBars()
	local spec = GetSpecialization()

	if spec == SPEC_WARLOCK_DESTRUCTION or spec == SPEC_WARLOCK_AFFLICTION then
		for i = 1, 4 do
			if i == 1 then
				self[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 4) - 2)
			else
				self[i]:Size(HUDOptions.ClassBarWidth, (HUDOptions.ClassBarHeight / 4) - 1)
			end
		end
	elseif spec == SPEC_WARLOCK_DEMONOLOGY then 
		self[1]:Size(HUDOptions.ClassBarWidth, HUDOptions.ClassBarHeight)
	end
end

function AH.RogueBarDisplay(self)
	local ab = self.AnticipationBar
	local bg = self.BanditsGuile
	if ab and ab.backdrop:IsShown() then
		if bg and bg.backdrop:IsShown() then
			bg:ClearAllPoints()
			bg:Point("RIGHT", ab, "LEFT", -7, 0)
		end
	else
		if bg.backdrop:IsShown() then
			bg:ClearAllPoints()
			bg:Point("RIGHT", self, "LEFT", -7, 0)
		end
	end
end