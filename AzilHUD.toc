## Interface: 50300
## Title: |cFFFF7D0AAzil|rHUD
## Author: Azilroka, Sortokk
## Version: 3.00
## Notes: Hud frames for Tukui, AsphyxiaUI, DuffedUI
## OptionalDeps: Tukui, AsphyxiaUI, DuffedUI, DSM
## SavedVariables: HUDOptions
## X-Tukui-ProjectID: 112
## X-Tukui-ProjectFolders: AzilHUD

Libs\Init.xml
Media\Media.lua
Init.lua
oUF.lua
Construct.lua
Updates.lua
HUD.lua
Options.lua