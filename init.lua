﻿if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local A, C = unpack(Tukui or AsphyxiaUI or DuffedUI)
local AddOnName, Engine = ...
local AddOn = {}
Engine[1] = AddOn
_G[AddOnName] = Engine

AddOn.BarPath = "Interface\\AddOns\\"..AddOnName.."\\Media\\StatusBars\\"
AddOn.IconPath = "Interface\\AddOns\\"..AddOnName.."\\Media\\Icons\\"
AddOn.MyClass = select(2, UnitClass("player"))
AddOn.PixelFont = C["Media"].PixelFont
AddOn.UFFont = C["Media"].AltFont
AddOn.Font = C["Media"].Font
AddOn.NormTex = C["Media"].Normal
AddOn.oUF = Engine.oUF
AddOn.Title = select(2, GetAddOnInfo(AddOnName))
AddOn.Version = GetAddOnMetadata(AddOnName, "Version")