if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local AH = unpack(select(2,...))
local oUF = AH.oUF

local HUDOptionsFrame = CreateFrame("Frame", "HUDOptionsFrame", UIParent)
HUDOptionsFrame:Hide()
HUDOptionsFrame:Point("CENTER", UIParent, "CENTER", 0, 0)
HUDOptionsFrame:Size(500)
HUDOptionsFrame:SetTemplate("Transparent")
HUDOptionsFrame:SetClampedToScreen(true)
HUDOptionsFrame:SetMovable(true)
HUDOptionsFrame:FontString("text", AH.Font, 14, "OUTLINE")
HUDOptionsFrame.text:SetPoint("TOP", HUDOptionsFrame, 0, -6)
HUDOptionsFrame.text:SetText(AH.Title.."Options - Version "..AH.Version)
HUDOptionsFrame:EnableMouse(true)
HUDOptionsFrame:RegisterForDrag("LeftButton")
HUDOptionsFrame:SetScript("OnDragStart", function(self) self:StartMoving() end)
HUDOptionsFrame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing() end)

local function CreateSlider(name, text, min, max, step)
	local frame = CreateFrame("Slider", "HUD"..name.."Slider", HUDOptionsFrame, "OptionsSliderTemplate")
	local frame2 = CreateFrame("EditBox", "HUD"..name.."EditBox", frame)
	frame:SetSize(100, 15)
	frame:SetOrientation("HORIZONTAL")
	frame:SetMinMaxValues(min, max)
	frame:SetValueStep(step)
	_G[frame:GetName().."Low"]:SetText(min)
	_G[frame:GetName().."High"]:SetText(max)
	frame:SetScript("OnShow", function(self)
		_G[self:GetName().."Text"]:SetText(text..HUDOptions[name])
		self:SetValue(HUDOptions[name])
	end)
	frame:HookScript("OnValueChanged", function(self, value) HUDOptions[name] = value frame2:SetText(HUDOptions[name]) _G[self:GetName().."Text"]:SetText(text..HUDOptions[name]) end)
	--frame:SkinSlideBar(10, true)
	
	frame2:SetAutoFocus(false)
	frame2:SetMultiLine(false)
	frame2:SetWidth(30)
	frame2:SetHeight(20)
	frame2:SetMaxLetters(3)
	frame2:SetTextInsets(3,0,0,0)
	frame2:SetFontObject(GameFontHighlight)
	frame2:SetPoint("TOP", frame, "BOTTOM", 0, -2)
	frame2:SetScript("OnShow", function(self) self:SetText(HUDOptions[name]) end)
	frame2:SetScript("OnEscapePressed", function(self) self:ClearFocus() self:SetText(HUDOptions[name]) end)
	frame2:SetScript("OnEnterPressed", function(self)
		self:ClearFocus()
		HUDOptions[name] = self:GetText()
		frame:SetValue(HUDOptions[name])
	end)
	frame2:SetTemplate("Default")
end

CreateSlider("HealthHeight", "Health Height: ", 75, 300, 1)
HUDHealthHeightSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -80)
HUDHealthHeightSlider:HookScript("OnValueChanged", function(self, value)
	for num, object in pairs(HUDNames) do
		if num == 1 or num == 2 then
			_G[object.."_Health"]:Height(HUDOptions["HealthHeight"])
		else
			_G[object.."_Health"]:Height(HUDOptions["HealthHeight"]*.75)
		end
	end
end)
	
CreateSlider("HealthWidth", "Health Width: ", 2, 50, 1)
HUDHealthWidthSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -130)
HUDHealthWidthSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(HUDNames) do
		_G[object.."_Health"]:Width(HUDOptions["HealthWidth"])
	end
end)

CreateSlider("PowerHeight", "Power Height: ", 75, 300, 1)
HUDPowerHeightSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -180)
HUDPowerHeightSlider:HookScript("OnValueChanged", function(self, value)
	for num, object in pairs(HUDNames) do
		if num == 1 or num == 2 then
			_G[object.."_Power"]:Height(HUDOptions["PowerHeight"])
		else
			_G[object.."_Power"]:Height(HUDOptions["PowerHeight"]*.75)
		end
		if _G[object.."_Experience"] then _G[object.."_Experience"]:Height(HUDOptions["PowerHeight"]) end
		if _G[object.."_Reputation"] then _G[object.."_Reputation"]:Height(HUDOptions["PowerHeight"]) end
	end
end)

CreateSlider("PowerWidth", "Power Width: ", 2, 50, 1)
HUDPowerWidthSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -230)
HUDPowerWidthSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(HUDNames) do
		_G[object.."_Power"]:Width(HUDOptions["PowerWidth"])
		if _G[object.."_Experience"] then _G[object.."_Experience"]:Width(HUDOptions["PowerWidth"]) end
		if _G[object.."_Reputation"] then _G[object.."_Reputation"]:Width(HUDOptions["PowerWidth"]) end
	end
end)

CreateSlider("ClassBarHeight", "Class Bar Height: ", 75, 300, 1)
HUDClassBarHeightSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -280)
HUDClassBarHeightSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(ClassBar) do
		_G[object]:Height(HUDOptions["ClassBarHeight"])
	end
	for _, object in pairs(ClassBarElements) do
		if #ClassBarElements == 1 then
			_G[object]:Height(HUDOptions["ClassBarHeight"])
		else
			_G[object]:Height((HUDOptions["ClassBarHeight"] - (#ClassBarElements - 1)) / #ClassBarElements)
		end
	end
end)

CreateSlider("ClassBarWidth", "ClassBar Width: ", 2, 50, 1)
HUDClassBarWidthSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -330)
HUDClassBarWidthSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(ClassBar) do
		_G[object]:Width(HUDOptions["ClassBarWidth"])
	end
	for _, object in pairs(ClassBarElements) do
		_G[object]:Width(HUDOptions["ClassBarWidth"])
	end
end)

CreateSlider("CastbarHeight", "Castbar Height: ", 75, 300, 1)
HUDCastbarHeightSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -380)
HUDCastbarHeightSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(HUDNames) do
		if _G[object.."_Castbar"] then
			_G[object.."_Castbar"]:Height(HUDOptions["CastbarHeight"])
		end
	end
end)
	
CreateSlider("CastbarWidth", "Castbar Width: ", 2, 50, 1)
HUDCastbarWidthSlider:SetPoint("TOPLEFT", HUDOptionsFrame, "TOPLEFT", 25, -430)
HUDCastbarWidthSlider:HookScript("OnValueChanged", function(self, value)
	for _, object in pairs(HUDNames) do
		if _G[object.."_Castbar"] then
			_G[object.."_Castbar"]:Width(HUDOptions["CastbarWidth"])
		end
	end
end)

local function CreateEditBox(name, width, height, maxletters, text)
	local frame = CreateFrame("EditBox", "HUD"..name.."EditBox", HUDOptionsFrame)
	frame:FontString("text", AH.Font, 12, "OUTLINE")
	frame.text:Point("BOTTOM", frame, "TOP", 0, 0)
	frame.text:SetText(text)
	frame:SetAutoFocus(false)
	frame:SetMultiLine(false)
	frame:Size(width, height)
	frame:SetMaxLetters(255)
	frame:SetTextInsets(3,0,0,0)
	frame:SetFontObject(GameFontHighlight)
	frame:SetTemplate("Default")
	frame:SetScript("OnShow", function(self) self:SetText(HUDOptions[name]) end)
	frame:SetScript("OnEscapePressed", function(self) self:ClearFocus() self:SetText(HUDOptions[name]) end)
	frame:SetScript("OnEnterPressed", function(self)
		self:ClearFocus()
		HUDOptions[name] = self:GetText()
	end)
end

CreateEditBox("Font", 320, 20, 255, "Font")
HUDFontEditBox:Point("BOTTOM", HUDOptionsFrame, "BOTTOM", 60, 100)

CreateEditBox("FontSize", 30, 20, 3, "Font Size")
HUDFontSizeEditBox:SetPoint("TOPRIGHT", HUDFontEditBox, "BOTTOMRIGHT", 0, -20)

CreateEditBox("FeedbackSize", 30, 20, 3, "Feedback Size")
HUDFeedbackSizeEditBox:SetPoint("TOPLEFT", HUDFontEditBox, "BOTTOMLEFT", 0, -20)

CreateEditBox("FontFlag", 150, 20, 255, "Font Flag")
HUDFontFlagEditBox:SetPoint("TOP", HUDFontEditBox, "BOTTOM", 0, -20)

CreateEditBox("Texture", 100, 20, 255, "Bar Texture")
HUDTextureEditBox:SetPoint("TOP", HUDOptionsFrame, "TOP", 0, -80)

local function CreateButton(name, btntext)
	local frame = CreateFrame("Button", "HUD"..name.."Button", HUDOptionsFrame)
	frame:Size(16)
	frame:CreateBackdrop()
	frame:SetBackdrop({bgFile = AH.NormTex, tile = false, tileSize = 0})
	frame:FontString("text", AH.Font, 12, "OUTLINE")
	frame.text:SetPoint("RIGHT", frame, "LEFT", -10, 0)
	frame.text:SetText(btntext)
	frame:SetScript("OnShow", function(self)
		if HUDOptions[name] then
			self:SetBackdropColor(0.11,0.66,0.11,1)
		else
			self:SetBackdropColor(0.68,0.14,0.14,1)
		end
	end)
	frame:SetScript("OnClick", function(self)
		if HUDOptions[name] then
			HUDOptions[name] = false
			self:SetBackdropColor(0.68,0.14,0.14,1)
		else
			HUDOptions[name] = true
			self:SetBackdropColor(0.11,0.66,0.11,1)
		end
	end)
end

CreateButton("ShowValues", "Show Health / Power %")
HUDShowValuesButton:Point("TOPRIGHT", HUDOptionsFrame, "TOPRIGHT", -25, -130)

CreateButton("HideUF", "Disable Default Unitframes")
HUDHideUFButton:Point("TOPRIGHT", HUDOptionsFrame, "TOPRIGHT", -25, -230)
HUDHideUFButton:HookScript("OnClick", function(self)
	for _, object in pairs(OtherUnitFrames) do
		if HUDOptions["HideUF"] then
			if _G[object] then _G[object]:Disable() end
		else
			if _G[object] then _G[object]:Enable() end
		end
	end
end)

local HUDOptionsFrameCloseButton = CreateFrame("Button", "HUDOptionsFrameCloseButton", HUDOptionsFrame)
HUDOptionsFrameCloseButton:SetPoint("TOP", HUDOptionsFrame, "BOTTOM", 0, -2)
HUDOptionsFrameCloseButton:Width(HUDOptionsFrame:GetWidth())
HUDOptionsFrameCloseButton:Height(20)
HUDOptionsFrameCloseButton:SetTemplate()
HUDOptionsFrameCloseButton:FontString("text", AH.Font, 12, "OUTLINE")
HUDOptionsFrameCloseButton.text:SetPoint("CENTER")
HUDOptionsFrameCloseButton.text:SetText("Close")
HUDOptionsFrameCloseButton:SetScript("OnClick", function(self) HUDOptionsFrame:Hide() end)