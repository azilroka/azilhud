if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local AH = unpack(select(2,...))
local oUF = AH.oUF

oUF.Tags.Events['HealthPercent'] = 'UNIT_HEALTH UNIT_MAXHEALTH UNIT_HEALTH_FREQUENT'
oUF.Tags.Methods['HealthPercent'] = function(unit)
	local max = UnitHealthMax(unit)
	if(max == 0) then
		return
	else
		return floor((UnitHealth(unit)/max) * 100)
	end
end

oUF.Tags.Events['PowerPercent'] = 'UNIT_POWER UNIT_MAXPOWER UNIT_POWER_FREQUENT'
oUF.Tags.Methods['PowerPercent'] = function(unit)
	local max = UnitPowerMax(unit)
	if(max == 0) then
		return
	else
		return floor((UnitPower(unit)/max) * 100)
	end
end

oUF.Tags.Events['NameColor'] = 'UNIT_POWER'
oUF.Tags.Methods['NameColor'] = function(unit)
	local color
	local Reaction = UnitReaction(unit, 'player')
	if UnitIsPlayer(unit) then
		color = oUF.colors.class[select(2, UnitClass(unit))]
	elseif Reaction then
		color = oUF.colors.reaction[Reaction]
	else
		color = { 1, 1, 1 }
	end	
	return format('|cff%02x%02x%02x', color[1] * 255, color[2] * 255, color[3] * 255)
end

oUF.Tags.Events['DifficultyColor'] = 'UNIT_LEVEL PLAYER_LEVEL_UP'
oUF.Tags.Methods['DifficultyColor'] = function(unit)
	local r, g, b
	local level = UnitLevel(unit)
	if level < 1 then
		r, g, b = 0.69, 0.31, 0.31
	else
		local DiffColor = UnitLevel('target') - UnitLevel('player')
		if DiffColor >= 5 then
			r, g, b = 0.69, 0.31, 0.31
		elseif DiffColor >= 3 then
			r, g, b = 0.71, 0.43, 0.27
		elseif DiffColor >= -2 then
			r, g, b = 0.84, 0.75, 0.65
		elseif -DiffColor <= GetQuestGreenRange() then
			r, g, b = 0.33, 0.59, 0.33
		else
			r, g, b = 0.55, 0.57, 0.61
		end
	end
	return format('|cff%02x%02x%02x', r * 255, g * 255, b * 255)
end

oUF.Tags.Events['PlayerStatus'] = 'PLAYER_FLAGS_CHANGED'
oUF.Tags.Methods['PlayerStatus'] = function(unit)
	local status, r, g, b = " ", 0, 0, 0
	if UnitIsAFK(unit) then
		status = CHAT_FLAG_AFK
		r, g, b = 1, .96, .45
	elseif UnitIsDND(unit) then
		status = CHAT_FLAG_DND
		r, g, b = .77, .12, .23
	elseif UnitIsDead(unit) then
		status = DEAD
		r, g, b = .77, .12, .23
	elseif UnitIsGhost(unit) then
		status = 'Ghost'
		r, g, b = .77, .12, .23
	elseif not UnitIsConnected(unit) then
		status = FRIENDS_LIST_OFFLINE
		r, g, b = .49, .52, .54
	end
	return format('|cff%02x%02x%02x'..status..'|r', r * 255, g * 255, b * 255)
end

oUF.colors.tapped = {
	0.55, 0.57, 0.61
}

oUF.colors.disconnected = {
	0.84, 0.75, 0.65
}

oUF.colors.power = {
	["MANA"] = {0.31, 0.45, 0.63},
	["RAGE"] = {0.69, 0.31, 0.31},
	["FOCUS"] = {0.71, 0.43, 0.27},
	["ENERGY"] = {0.65, 0.63, 0.35},
	["RUNES"] = {0.55, 0.57, 0.61},
	["RUNIC_POWER"] = {0, 0.82, 1},
	["AMMOSLOT"] = {0.8, 0.6, 0},
	["FUEL"] = {0, 0.55, 0.5},
	["POWER_TYPE_STEAM"] = {0.55, 0.57, 0.61},
	["POWER_TYPE_PYRITE"] = {0.60, 0.09, 0.17},
}

oUF.colors.runes = {
	[1] = {.69,.31,.31},
	[2] = {.33,.59,.33},
	[3] = {.31,.45,.63},
	[4] = {.84,.75,.65},
}

oUF.colors.reaction = {
	[1] = { 222/255, 95/255,  95/255 }, -- Hated
	[2] = { 222/255, 95/255,  95/255 }, -- Hostile
	[3] = { 222/255, 95/255,  95/255 }, -- Unfriendly
	[4] = { 218/255, 197/255, 92/255 }, -- Neutral
	[5] = { 75/255,  175/255, 76/255 }, -- Friendly
	[6] = { 75/255,  175/255, 76/255 }, -- Honored
	[7] = { 75/255,  175/255, 76/255 }, -- Revered
	[8] = { 75/255,  175/255, 76/255 }, -- Exalted	
}

oUF.colors.class = {
	["DEATHKNIGHT"] = { 196/255,  30/255,  60/255 },
	["DRUID"]       = { 255/255, 125/255,  10/255 },
	["HUNTER"]      = { 171/255, 214/255, 116/255 },
	["MAGE"]        = { 104/255, 205/255, 255/255 },
	["PALADIN"]     = { 245/255, 140/255, 186/255 },
	["PRIEST"]      = { 212/255, 212/255, 212/255 },
	["ROGUE"]       = { 255/255, 243/255,  82/255 },
	["SHAMAN"]      = {  41/255,  79/255, 155/255 },
	["WARLOCK"]     = { 148/255, 130/255, 201/255 },
	["WARRIOR"]     = { 199/255, 156/255, 110/255 },
	["MONK"]        = { 0/255, 255/255, 150/255   },
}