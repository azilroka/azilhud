--[[

]]

--[[ GENERAL
	7353, 			-- Gem�tliches Feuer
	17619, 			-- Alchemistenstein
	72968, 			-- Schatz' Schleife
	97340,			-- Gildenchampion
	97341,			-- Gildenchampion
	-- Allianz (Wappenr�cke)
	93795,			-- Champion von Sturmwind
	93805, 			-- Champion von Eisenschmiede
	93806, 			-- Champion von Darnassus
	93811, 			-- Champion der Exodar
	93816, 			-- Champion von Gilneas
	93821, 			-- Champion von Gnomeregan
	-- Horde (Wappenr�cke)
	93825, 			-- Champion von Orgrimmar
	93827, 			-- Champion der Dunkelspeere
	93828, 			-- Champion von Silbermond
	93830, 			-- Champion des Bilgewasserkartells
	94462,			-- Champion von Unterstadt
	94463, 			-- Champion von Donnerfels
	-- Cataclysm (Wappenr�cke)
	93337, 			-- Champion von Ramkahen
	93339,			-- Champion des Irdenen Rings
	93341, 			-- Champion der W�chter des Hyjal
	93347, 			-- Champion von Therazane
	93368, 			-- Champion des Wildhammerklans
	94158, 			-- Champion des Drachenmalklans
	-- WotLK (Wappenr�cke)
	57820, 			-- Champion der Schwarzen Klinge
	57821, 			-- Champion der Kirin Tor
	57822, 			-- Champion des Wyrmruhtempels
	-- Volk
	59542, 			-- Gabe der Naaru
	-- Berufe
	55500, 			-- Lebensblut
	74497, 			-- Lebensblut
	-- Verzauberungen / Items
	74224, 			-- Gesang des Herzens
	74241, 			-- Machtstrom
	74243, 			-- Windwandeln
	75170,			-- Leutgarn
	-- Flasks
	79471, 			-- Fl�schchen der Winde
	92729, 			-- Fl�schchen der Stahlhaut
	92730, 			-- Fl�schchen der drakonischen Gedanken
	-- Trinkets
	91141, 			-- Schlachthymne
	91155, 			-- Wache Seele
	-- Mounts
	783,			-- Reisegestalt
	1066,			-- Wassergestalt
	6648,			-- Kastanienbraune Stute
	8394,			-- Gestreifter Frosts�bler
	10789,			-- Gefleckter Frosts�bler
	10793, 			-- Gestreifter Nachts�bler
	13819,			-- Schlachtross beschw�ren
	23221,			-- Schneller Frosts�bler
	23240,			-- Schneller wei�er Widder
	23241,			-- Schneller blauer Raptor
	23242,			-- Schneller olivfarbener Raptor
	23243,			-- Schneller orangefarbener Raptor
	23246,			-- Purpurnes Skelettschlachtross
	23247,			-- Gro�er wei�er Kodo
	23248,			-- Gro�er grauer Kodo
	32235,			-- Goldener Greif
	32240,			-- Wei�er Greif
	32242,			-- Schneller blauer Greif
	32289,			-- Schneller roter Greif
	33943,			-- Fluggestalt
	39315,			-- Kobaltblauer Reittalbuk
	40212,			-- Netherdrache des Drachenmals
	43927,			-- Cenarischer Kriegshippogryph
	44151, 			-- Turbogetriebene Flugmaschine
	48025,			-- Ross des kopflosen Reiters
	48778,			-- Todesstreitross von Acherus
	54729,			-- Gefl�gelter Greif der Schwarzen Klinge
	59568,			-- Blaudrache
	59650,			-- Schwarzdrache
	59785,			-- Schwarzes Kriegsmammut
	60024,			-- Violetter Protodrache
	60025,			-- Albinodrache
	60424, 			-- Chopper des Robogenieurs
	61294,			-- Gr�ner Protodrache
	61425,			-- Tundramammut des Reisenden
	61451,			-- Fliegender Teppich
	61996,			-- Blauer Drachenfalke
	63637,			-- Darnassischer Nachts�bler
	63956,			-- Eisenbeschlagener Protodrache
	65638,			-- Schneller Monds�bler
	66087,			-- Hippogryph des Silberbunds
	66846,			-- Ockerfarbenes Skelettschlachtross
	66847,			-- Gestreifter D�mmers�bler
	69395, 			-- Drache von Onyxia
	72807,			-- Eisbeschlagener Frostbrutbezwinger
	87840,			-- Wilde Hatz
	88331,			-- Vulkansteindrache
	88741,			-- Drache des Westwinds
	88990,			-- Dunkler Ph�nix
	93326,			-- Sandsteindrache
	98727,			-- Gefl�gelter W�chter
	-- Essen (Satt)
	24870, 
	33259, 33261, 33263, 
	43771, 
	57079, 57097, 57100, 57102, 57107, 57111, 57139, 57286, 57288, 57294, 
	57325, 57327, 57329, 57332, 57334, 57356, 57358, 57360, 57367, 57371, 
	57399,
	65412, 66623, 
	87545, 87546, 87547, 87548, 87549, 87550, 87551, 87552, 87554, 87555,
	87556, 87557, 87558, 87559, 87560, 87561, 87562, 87563, 87564, 87634,
	87635,
]]

--[[
	95223,			-- K�rzlich Masssen-wiederbelebt
]]

--[[ WARRIOR
	-- Eigene Buffs
	871, 			-- Schildwall
	2565, 			-- Schildblock
	6673, 			-- Schlachtruf
	12964, 			-- Kampftrance
	12976, 			-- Letztes Gefecht
	16491, 			-- Blutwahnsinn
	18499, 			-- Berserkerwut
	29801, 			-- Toben
	50227, 			-- Schwert und Schild
	50720, 			-- Wachsamkeit
	52437, 			-- Pl�tzlicher Tod
	55694, 			-- W�tende Regeneration
	57516, 			-- Wutanfall
	57519, 			-- Wutanfall
	85730, 			-- T�dliche Stille
	97463, 			-- Anspornender Schrei
	87096, 			-- Vom Donner ger�hrt
	-- Eigene Debuffs
	6343, 			-- Donnerknall?
	12294, 			-- T�dlicher Sto�?
	12721, 			-- Tiefe Wunden
	18498, 			-- Zum Schweigen gebracht - Auf die Kehle zielen
]]

--[[ MAGE
	-- Eigene Buffs
	6117, 			-- Magische R�stung
	12544, 			-- Frostr�stung
	18100, 			-- Frostr�stung
	30482, 			-- Gl�hende R�stung
	77977, 			-- Frostr�stung
]]

--[[ HUNTER
	-- Eigene Buffs
	5118,			-- Aspekt des Geparden
	13159,			-- Aspekt des Rudels
	13165, 			-- Aspekt des Falken
	20043, 			-- Aspekt der Wildnis
	24529, 			-- Geistbande
	53257, 			-- Kobrast��e
	75447, 			-- Wilde Inspiration
	-- Eigene Debuffs
	1130,			-- Mal des J�gers
	-- Pet
	136,			-- Tier heilen
]]

--[[ DRUID
	-- Eigene Buffs
	783,			-- Reisegestalt
	1066,			-- Wassergestalt
	33943,			-- Fluggestalt
]]

--[[ PALADIN
	-- Eigene Buffs
	465, 			-- Aura der Hingabe
	8258, 			-- Aura der Hingabe
	17232, 			-- Aura der Hingabe
	7294, 			-- Aura der Vergeltung
	79976, 			-- Aura der Vergeltung
	19746, 			-- Aura der Konzentration
	32223, 			-- Aura des Kreuzfahrers
	1044, 			-- Hand der Freiheit
	20164, 			-- Siegel der Gerechtigkeit
	20165, 			-- Siegel der Einsicht
	20170, 			-- Siegel der Gerechtigkeit
	25780, 			-- Zorn der Gerechtigkeit
	31801, 			-- Siegel der Wahrheit
	31821, 			-- Aurenbeherrschung
	31850, 			-- Unerm�dlicher Verteidiger
	31884, 			-- Zornige Vergeltung
	31930, 			-- Richturteil des Weisen
	53657, 			-- Richturteile des Reinen
	54149, 			-- Lichtenergie
	54428, 			-- G�ttliche Bitte
	79102, 			-- Segen der Macht
	79968, 			-- Segen der K�nige
	79977, 			-- Segen der Macht
	84963, 			-- Inquisition
	85416, 			-- Oberster Kreuzfahrer
	85433, 			-- Heilige Pflicht
	86669, 			-- W�chter der Uralten K�nige
	87342, 			-- Heiliger Schild
	-- Eigene Debuffs
	853, 			-- Hammer der Gerechtikeit
	20154, 			-- Siegel der Rechtschaffenheit
	20170, 			-- Siegel der Gerechtigkeit
	26017, 			-- Rechtschaffene Schw�chung
	31803, 			-- Tadel
	31935, 			-- Schild des R�chers
	79965, 			-- Schild des R�chers
	62124, 			-- Hand der Abrechnung
	68055, 			-- Richturteil des Gerechten
]]

--[[

	15473,			-- Schattengestalt
]]
local ADDON_NAME, ns = ...

	local settings = {}

	settings.supportedUnits = {
		['player'] = true,
		['target'] = true,
		['focus'] = true,
		['pet'] = true,
	}

	settings.PvEBossDebuffs = {
		-- 8% Spell dmg
		[1490] = true,			-- Curse of the Elements (Warlock)
		[60433] = true, 		-- Earth and Moon (Druid (Balance))
		[93068] = true,			-- Master Poisoner (Rogue (Assassination))
		[65142] = true, 		-- Ebon Plague (Deathknight (Unholy))
		[24844] = true, 		-- Lightning Breath (Hunter (Wind Serpent))
		[34889] = true,			-- Fire Breath (Hunter (Dragonhawk))
		-- 5% Spell crit
		[22959] = true,			-- Critical Mass (Mage (Fire))
		[17800] = true, 		-- Shadow and Flame (Warlock (Destruction))
		-- 4% Melee dmg
		[30070] = true, 		-- Blood Frenzy (Warrior (Arms))
		[58683] = true,			-- Savage Combat (Rogue (Combat))
		[81326] = true,			-- Brittle Bones (Deathknight (Frost))
		[50518] = true,			-- Ravage (Hunter (Ravager))
		[55749] = true,			-- Acid Spit (Hunter (Worm))
		-- 12% reduced armor
		[91565] = true,			-- Faerie Fire (Druid)
		[58567] = true, 		-- Sunder Armor (Warrior)
		[8647] = true,			-- Expose Armor (Rogue)
		[35387] = true,			-- Corrosive Spit (Hunter (Serpent))
		[50498] = true, 		-- Tear Armor (Hunter (Raptor))
		-- 20% reduced attack speed
		[58180] = true,			-- Infected Wounds (Druid (Feral))
		[6343] = true, 			-- Thunderclap (Warrior)
		[55095] = true,			-- Frost Fever (Deathknight)
		[68044] = true,			-- Judgements of the Just (Paladin (Protection))
		[0] = true, 			-- Waylay (Rogue (Subtlety))
		[0] = true,				-- Earth Shock (Shaman)
		[90314] = true, 		-- Tailspin (Hunter (Fox))
		[50285] = true,			-- Dust Cloud (Hunter (Tailstrider))
		-- 10% reduced dmg done
		[99] = true,			-- Demoralizing Roar (Druid (Feral))
		[1160] = true,			-- Demoralizing Shout (Warrior (Protection))
		[81130] = true,			-- Scarlet Fever (Deathknight (Blood))
		[26017] = true, 		-- Vindication (Paladin (Protection))
		[0] = true, 			-- Curse of Weakness (Warlock)
		[50256] = true, 		-- Demoralizing Roar (Hunter (Bear))
		[24423] = true, 		-- Demoralizing Screech (Hunter (Carrion Bird))
		-- 30% increased bleed dmg
		[33876] = true,			-- Mangle (Cat) (Druid (Cat))
		[33878] = true, 		-- Mangle (Bear) (Druid (Bear))
		[46857] = true, 		-- Trauma (Warrior (Arms))
		[16511] = true, 		-- Hemorrhage (Rogue (Subtlety))
		[50271] = true, 		-- Tendon Rip (Hunter (Hyena))
		[35290] = true, 		-- Gore (Hunter (Boar))
		[57386] = true, 		-- Stampede (Hunter (Rhino))
		-- 10% reduced healing
		-- 30% reduced cast speed
	}

	ns.settings = settings

-- ************************************************************************************************
-- ***** LIB **************************************************************************************
-- ************************************************************************************************
	local lib = {}

	--[[ Debugging to ChatFrame
		VOID debugging(STRING text)
	]]
	lib.debugging = function(text)
		DEFAULT_CHAT_FRAME:AddMessage('|cffffd700oUF_BuffFilter:|r |cffeeeeee'..text..'|r')
	end

	lib.CreateUnitDefaults = function(unit)
		lib.debugging('creating defaults for '..unit)
		local tmp = {
			['buffs'] = {
				['mode'] = 'blacklist',
				['list'] = {
					[7353] = true, 			-- Gem�tliches Feuer
					[17619] = true,			-- Alchemistenstein
					[72968] = true,			-- Schatz' Schleife
					[97340] = true,			-- Gildenchampion
					[97341] = true,			-- Gildenchampion
					-- Allianz (Wappenr�cke)
					[93795] = true,			-- Champion von Sturmwind
					[93805] = true,			-- Champion von Eisenschmiede
					[93806] = true,			-- Champion von Darnassus
					[93811] = true,			-- Champion der Exodar
					[93816] = true,			-- Champion von Gilneas
					[93821] = true,			-- Champion von Gnomeregan
					-- Horde (Wappenr�cke)
					[93825] = true,			-- Champion von Orgrimmar
					[93827] = true,			-- Champion der Dunkelspeere
					[93828] = true,			-- Champion von Silbermond
					[93830] = true,			-- Champion des Bilgewasserkartells
					[94462] = true,			-- Champion von Unterstadt
					[94463] = true,			-- Champion von Donnerfels
					-- Cataclysm (Wappenr�cke)
					[93337] = true,			-- Champion von Ramkahen
					[93339] = true,			-- Champion des Irdenen Rings
					[93341] = true,			-- Champion der W�chter des Hyjal
					[93347] = true,			-- Champion von Therazane
					[93368] = true,			-- Champion des Wildhammerklans
					[94158] = true,			-- Champion des Drachenmalklans
					-- WotLK (Wappenr�cke)
					[57820] = true,			-- Champion der Schwarzen Klinge
					[57821] = true,			-- Champion der Kirin Tor
					[57822] = true,			-- Champion des Wyrmruhtempels
					-- Verzauberungen / Items
					[74241] = true,			-- Machtstrom
					[74243] = true,			-- Windwandeln
					[75170] = true,			-- Leutgarn
				}
			},
			['debuffs'] = {
				['mode'] = 'blacklist',
				['list'] = {}
			}
		}
		return tmp
	end

	--[[ Is a value in a table?
		BOOL in_array(MIXED e, TABLE t)
	]]
	lib.in_array = function(e, t)
		-- lib.debugging('entering in_array() with spellID='..e)
		-- for _,v in pairs(t) do
			-- if ( v == e ) then
				-- -- lib.debugging('in_array(): v == e: '..v..'/'..e)
				-- return true
			-- end
		-- end
		if ( t[e] ) then
			return true
		end
		return false
	end

	--[[ Collects an aura to the SavedVars
		VOID collectAura(INT id, STRING name)
	]]
	lib.collectAura = function(id, name)
		oUF_BuffFilter_AuraList[id] = name
	end

	--[[ Providing blacklisting. Returns "false" if the ID is found.
		BOOL FilterBlacklist(INT spellID, TABLE filterSRC)
	]]
	lib.FilterBlacklist = function(spellID, list)
		-- if ( #list ~= 0 ) then
			if ( lib.in_array(spellID, list) ) then
				return false
			end
		-- end
		return true
	end


	--[[ Providing whitelisting. Returns "true" if the ID is found.
		BOOL FilterWhitelist(INT spellID, TABLE filterSRC)
	]]
	lib.FilterWhitelist = function(spellID, list)
		-- lib.debugging('entering FilterWhitelist() with spellID='..spellID)
		-- if ( #list ~= 0 ) then
			if ( lib.in_array(spellID, list) ) then
				return true
			end
		-- end
		return false
	end

	--[[ Generic filter-function (distinction between blacklist and whitelist)
		BOOL FilterGeneric(INT spellID, TABLE filterSRC)
	]]
	lib.FilterGeneric = function(spellID, name, filterSRC, icon, caster)
		-- lib.debugging('entering FilterGeneric() with spellID='..spellID..' ('..name..')')
		lib.collectAura(spellID, name)
		icon.caster = caster
		icon.spellID = spellID
		if (filterSRC.mode == 'blacklist') then
			return lib.FilterBlacklist(spellID, filterSRC.list)
		else
			return lib.FilterWhitelist(spellID, filterSRC.list)
		end
	end
-- ************************************************************************************************
	ns.lib = lib
-- ************************************************************************************************

