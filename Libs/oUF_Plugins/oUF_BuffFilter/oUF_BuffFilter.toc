## Interface: 40200
## Author: Mischback
## X-URI: http://gihub.com/Mischback
## Title: oUF_|cff0099ffBuffFilter|r
## Version: BETA 0.9
## Notes: Provides a buff-/debuff-filtering engine for oUF
## X-Category: UnitFrame
## DefaultState: Disabled
## RequiredDeps: oUF
## SavedVariables: oUF_BuffFilter_AuraList
## SavedVariablesPerCharacter: oUF_BuffFilter_Settings

lib.lua
core.lua