--[[

]]
local ADDON_NAME, ns = ...

local settings = ns.settings
local lib = ns.lib

oUF_BuffFilter = CreateFrame('Frame', 'oUF_BuffFilter', UIParent)

function oUF_BuffFilter_Buffs(icons, unit, icon, name, rank, texture, count, dtype, duration, timeLeft, caster, isStealable, shouldConsolidate, spellID)
	if ( not settings.supportedUnits[unit] ) then
		lib.debugging('unsupported unit: '..unit)
		return true
	end

	if ( not oUF_BuffFilter_Settings[unit] ) then
		oUF_BuffFilter_Settings[unit] = lib.CreateUnitDefaults(unit)
	end

	return lib.FilterGeneric(spellID, name, oUF_BuffFilter_Settings[unit]['buffs'], icon, caster)
end

function oUF_BuffFilter_Debuffs(icons, unit, icon, name, rank, texture, count, dtype, duration, timeLeft, caster, isStealable, shouldConsolidate, spellID)
	if ( not settings.supportedUnits[unit] ) then
		lib.debugging('unsupported unit: '..unit)
		return true
	end

	if ( not oUF_BuffFilter_Settings[unit] ) then
		oUF_BuffFilter_Settings[unit] = lib.CreateUnitDefaults(unit)
	end

	return lib.FilterGeneric(spellID, name, oUF_BuffFilter_Settings[unit]['debuffs'], icon, caster)
end

function oUF_BuffFilter_PvEBossDebuffs(icons, unit, icon, name, rank, texture, count, dtype, duration, timeLeft, caster, isStealable, shouldConsolidate, spellID)
	lib.collectAura(spellID, name..'(seen on boss)')

	return lib.FilterWhitelist(spellID, settings.PvEBossDebuffs)
end

local f = CreateFrame('Frame', nil, UIParent)
f:RegisterEvent('ADDON_LOADED')
f:SetScript('OnEvent', function(self, event, addon)
	if ( addon ~= ADDON_NAME ) then return end

	if ( not oUF_BuffFilter_AuraList ) then
		oUF_BuffFilter_AuraList = {}
	end

	if ( not oUF_BuffFilter_Settings ) then
		oUF_BuffFilter_Settings = {}
	end
end)